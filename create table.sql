create table pedido (
	id NUMBER GENERATED ALWAYS AS IDENTITY,
	data timestamp,
	user_name varchar(200), 
	constraint pk_pedido primary key (id)
);

create table estoque (
	id NUMBER GENERATED ALWAYS AS IDENTITY,
    estoque_gondola int,
    estoque_real int,
    constraint pk_estoque primary key (id)
);

create table produto (
	id NUMBER GENERATED ALWAYS AS IDENTITY,
	nome varchar(200),
	valor decimal,
	estoque int,
	constraint pk_produto primary key (id)
);

alter table produto add constraint fk_esquetoque foreign key (estoque) references estoque(id);

create table item_pedido (
	id NUMBER GENERATED ALWAYS AS IDENTITY,
	quantidade int,
	id_pedido number,
	id_produto number,
	constraint pk_item_pedido primary key (id)
);

alter table item_pedido add constraint fk_item_pedido foreign key(id_pedido) references pedido(id);

alter table item_pedido add constraint fk_item_pedido_produto foreign key(id_produto) references produto(id);
