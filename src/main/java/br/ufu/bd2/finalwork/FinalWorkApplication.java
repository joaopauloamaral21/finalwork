package br.ufu.bd2.finalwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class FinalWorkApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalWorkApplication.class, args);
	}
}
