package br.ufu.bd2.finalwork.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufu.bd2.finalwork.domain.Usuario;

@RestController 
@RequestMapping(path="/usuario")
public class UsuarioController {
//	@Autowired 
//	private UsuarioRepository repositorio;
//
//	@Autowired 
//	private UsuarioService servico;
	
	@GetMapping
	public List<Usuario> buscar(@RequestParam Map<String, String> parametros,
    		@RequestParam(name = "page", defaultValue = "0") Integer page, 
    		@RequestParam(name = "size", defaultValue = "10") Integer size) throws SQLException {
//		return servico.buscar(parametros, page, size);
		
		String urlBancoDados = "jdbc:oracle:thin:@localhost:1521/xe";
		String user = "system";
		String password = "oracle";
		Connection con = DriverManager.getConnection(urlBancoDados, user, password);
		
		String qs = "select * from produto";
		
		Statement st = con.createStatement();
		
		ResultSet rs = st.executeQuery(qs);
		
		List<Usuario> retorno = new ArrayList<Usuario>(); 
		Usuario usuario;
		
		while (rs.next()) {
			usuario = new Usuario();
			usuario.setNome(rs.getString("nome"));
			usuario.setValor(rs.getBigDecimal("valor"));
			
			retorno.add(usuario);
		}
		
		return retorno;
	}
	
	@GetMapping(path="/{id}")
	public Usuario buscar(@PathVariable("id") Long id) {
//		return repositorio.findOne(id);
		return null;
	}
	
	@PostMapping
	public Usuario novo(@RequestBody Usuario usuario) {
//		return servico.save(usuario);
		return null;
	}

	@PutMapping
	public Usuario editar(@RequestBody Usuario usuario) {
//		if (usuario.getId() == null) {
//			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Id obrigatório");
//		}
//		
//		validacaoExistenciaUsuario(usuario.getId());
//		return repositorio.save(usuario);
		return null;
	}

	@DeleteMapping(path="/{id}")
	public void deletar(@PathVariable("id") Long id) {
//		validacaoExistenciaUsuario(id);
//		repositorio.delete(id);
	}
	
	private void validacaoExistenciaUsuario(Long id) {
//		if (!repositorio.exists(id)) {
//			throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "Inexistente");
//		}
	}
}