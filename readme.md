
## Tecnologias usadas
* SpringBoot 2.0
* Java 8
* Maven
* Eclipse
* Oracle
* Docker
* Postman


## Requerimentos

Para fazer o build da aplicação é necessário ter instalado:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)
- [Docker](https://store.docker.com/search?type=edition&offering=community)

[Sugestão de imagem do oracle 12c para o Docker] (https://hub.docker.com/r/sath89/oracle-12c/)

## Rodando a aplicação localmente

Existem várias formas de rodar uma aplicação Spring Boot localmente. Uma dessa formas é rodar o metodo  `main` da classe `br.ufu.bd2.FinalWorkApplication` na sua IDE.

Pode-se usar tambem o plugin [Spring Boot Maven plugin](https://marketplace.eclipse.org/content/spring-tools-aka-spring-ide-and-spring-tool-suite) e usar o Spring Dashboard para rodar a aplicação montcapital-dash-controller.

Além disso pode se executar o `mvn install` e rodar o .jar da pasta target.


